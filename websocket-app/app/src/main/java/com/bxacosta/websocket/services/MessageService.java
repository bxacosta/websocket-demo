package com.bxacosta.websocket.services;

import com.bxacosta.websocket.models.Message;
import com.bxacosta.websocket.stopm.StompMessage;
import com.bxacosta.websocket.stopm.StompMessageListener;
import com.bxacosta.websocket.stopm.StopmClient;
import com.bxacosta.websocket.stopm.TopicHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageService {

    private ObjectMapper mapper = new ObjectMapper();
    private static final String topic = "/app/ws";
    private StopmClient client;

    public MessageService() {
        client = new StopmClient();
        TopicHandler handler = client.subscribe("/topic/messages");
        handler.addListener(new StompMessageListener() {
            @Override
            public void onMessage(StompMessage message) {
                System.out.println("MESSAGE FROM: " + message.getHeader("destination") + " : " + message.getContent());
            }
        });
    }

    public void connect() {
        client.connect("ws://192.168.137.1:8080/ws/websocket");
    }

    public void send(Message message) {
        try {
            client.send(topic, mapper.writeValueAsString(message));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    public void disconnect() {
        client.disconnect();
    }

}
