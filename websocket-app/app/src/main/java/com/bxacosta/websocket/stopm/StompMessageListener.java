package com.bxacosta.websocket.stopm;

public interface StompMessageListener {
    void onMessage(StompMessage message);
}
