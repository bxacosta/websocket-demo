package com.bxacosta.websocket.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.bxacosta.websocket.R;
import com.bxacosta.websocket.models.Message;
import com.bxacosta.websocket.services.MessageService;
import com.bxacosta.websocket.stopm.StopmClient;
import com.bxacosta.websocket.stopm.StompMessage;
import com.bxacosta.websocket.stopm.StompMessageListener;
import com.bxacosta.websocket.stopm.TopicHandler;

public class MainActivity extends AppCompatActivity {

    private MessageService messageService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        messageService = new MessageService();
    }

    public void sendMessage(View view) {
        if (messageService.isConnected()) {
            messageService.send(Message.builder().from("android").text("Hola").build());
        } else {
            Toast.makeText(this, "Debe conectarse al WebSocket primero", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_connect) {
            if (messageService.isConnected()) {
                Toast.makeText(this, "Ya esta conectado al WebSocket", Toast.LENGTH_SHORT).show();
            } else {
                messageService.connect();
                Toast.makeText(this, "Conectando al WebSocket", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        messageService.disconnect();
    }
}
