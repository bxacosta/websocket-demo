package com.bxacosta.websocket.controllers;

import com.bxacosta.websocket.models.Message;
import com.bxacosta.websocket.models.MessageEvent;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@CommonsLog
@RestController
public class MessageController {

    private final SimpMessagingTemplate template;

    public MessageController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/ws")
    @SendTo("/topic/messages/")
    public MessageEvent send(Message message) throws Exception {
        log.info("MESSAGE: " + message.toString());
        return new MessageEvent(new Date(), message);
    }

    @GetMapping("/test")
    public void test() {
        template.convertAndSend("/topic/messages",
                Message.builder().from("spring").text("Hola que tal").build());
    }
}
